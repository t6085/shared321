package SharedModels


type Enrollment struct {
	FirstName      string `json:"firstname"`
	LastName       string `json:"lastname"`
	CompanyName    string `json:"companyname"`
	PrefLanguage   string `json:"PreferredLanguage"`
	MailingStreet1 string `json:"mailingstreet1"`
	MailingStreet2 string `json:"mailingstreet2"`
	MailingCity    string `json:"mailingcity"`
	MailingState   string `json:"mailingstate"`
	MailingZip     string `json:"mailingzip"`
	MailingCountry string `json:"mailingcountry"`
	BillingStreet1 string `json:"billingstreet1"`
	BillingStreet2 string `json:"billingstreet2"`
	BillingCity    string `json:"billingcity"`
	BillingState   string `json:"billingstate"`
	BillingZip     string `json:"billingzip"`
	BillingCountry string `json:"billingcountry"`
	Email          string `json:"email"`
	AltEmail       string `json:"altemail"`
	Phone          string `json:"Phone"`
	PaymentMethod  string `json:"paymentmethod"`
	CCnumber       string `json:"CCnumber"`
	ExpDate        string `json:"expdate"`
	PromoCode      string `json:"promocode,omitempty"`
	ProductCode    string `json:"productcode"`
	CVC			   string `json:"CVC"`
	Password       string `json:"Password"`
}
